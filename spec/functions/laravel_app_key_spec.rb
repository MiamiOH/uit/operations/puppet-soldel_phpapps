require 'spec_helper'

describe 'laravel_app_key', type: :puppet_function do
  it do
    # rubocop:disable RSpec/NamedSubject
    expect(subject.execute).to match(%r(^base64:.{32,}$))
    # rubocop:enable RSpec/NamedSubject
  end
end
