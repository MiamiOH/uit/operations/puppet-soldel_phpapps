require 'spec_helper'

describe 'soldel_phpapps::connections::database', type: :define do
  let(:title) { 'testapp' }

  on_supported_os.each do |os, facts|
    context "on #{os}" do
      let(:facts) do
        facts
      end

      let :pre_condition do
        'concat{"env":
            path => "/tmp/env",
         }'
      end

      context 'with defaults' do
        it { is_expected.to compile.with_all_deps }
      end
    end
  end
end
