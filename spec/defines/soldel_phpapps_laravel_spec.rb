require 'spec_helper'

describe 'soldel_phpapps::laravel', type: :define do
  let(:title) { 'testapp' }

  on_supported_os.each do |os, facts|
    let :pre_condition do
      'service{"filebeat":
          ensure => "running",
       }'
    end

    context "on #{os}" do
      let(:facts) do
        facts
      end

      context 'with defaults' do
        it { is_expected.to compile.with_all_deps }
      end
      context 'without ssh_keypair set' do
        it { is_expected.not_to contain_file('/var/www/storage/testapp/app/.ssh') }
        it { is_expected.not_to contain_file('/var/www/storage/testapp/app/.ssh/id_rsa') }
        it { is_expected.not_to contain_file('/var/www/storage/testapp/app/.ssh/id_rsa.pub') }
        it { is_expected.not_to contain_file('/var/www/storage/testapp/app/.ssh/keylabel_id_rsa') }
        it { is_expected.not_to contain_file('/var/www/storage/testapp/app/.ssh/keylabel_id_rsa.pub') }
      end

      context 'with ssh_keypair containing keylabel' do
        let(:params) { { app_user: 'testapp', ssh_keypair: { 'keylabel' => { 'public' => 'test', 'private' => 'test' } } } }

        it { is_expected.to contain_file('/var/www/storage/testapp/app/.ssh') }
        it { is_expected.to contain_file('/var/www/storage/testapp/app/.ssh/keylabel_id_rsa').with_content('test') }
        it { is_expected.to contain_file('/var/www/storage/testapp/app/.ssh/keylabel_id_rsa.pub').with_content('test') }
      end

      context 'with ssh_keypair default name' do
        let(:params) { { app_user: 'testapp', ssh_keypair: { 'default' => { 'public' => 'test', 'private' => 'test' } } } }

        it { is_expected.to contain_file('/var/www/storage/testapp/app/.ssh') }
        it { is_expected.to contain_file('/var/www/storage/testapp/app/.ssh/id_rsa').with_content('test') }
        it { is_expected.to contain_file('/var/www/storage/testapp/app/.ssh/id_rsa.pub').with_content('test') }
      end

      context 'without queue_enabled' do
        it { is_expected.to contain_systemd__unit_file('laravel-queue-testapp.service').with_ensure('absent') }
        it { is_expected.to contain_service('laravel-queue-testapp').with_ensure('stopped') }
      end

      context 'with queue_enabled without options' do
        let(:params) { { queue_enabled: true } }

        it { is_expected.to contain_systemd__unit_file('laravel-queue-testapp.service').with_ensure('present') }
        it { is_expected.to contain_service('laravel-queue-testapp').with_ensure('running') }
      end

      context 'with queue_enabled with options' do
        let(:params) { { queue_enabled: true, queue_options: '--tries=3 --delay 600' } }

        it { is_expected.to contain_systemd__unit_file('laravel-queue-testapp.service').with_ensure('present') }
        it { is_expected.to contain_systemd__unit_file('laravel-queue-testapp.service').with_content(%r{--tries=3 --delay 600}) }
        it { is_expected.to contain_service('laravel-queue-testapp').with_ensure('running') }
      end
    end
  end
end
