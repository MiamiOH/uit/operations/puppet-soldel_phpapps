# Changelog

2023-06-02 (1.7.2)
        * Another try to fix ordering by requiring app install before public storage link
        * Update dependencies

2023-05-31 (1.7.1)
        * Fix ordering for public directory link.
        * Remove validate_hash

2022-11-02 (1.7.0)
        * Support configuring app without an RPM (allows using .env in containers)

2022-09-29 (1.6.0)
        * Add APP_NAME to Laravel .env files

2022-02-08 (1.5.12)
        * update pdk version added cron dependency

2021-07-02 (1.5.11)
        * update systemd version

2021-04-27 (1.5.10)
        * Switc JWT params to bool and string value

2021-04-27 (1.5.9)
        * Convert JWT param to hash to give more flexibility

2021-04-27 (1.5.8)
        * Add handling of JWT secret in env file

2021-03-22 (1.5.7)
        * Update filebeat to generate app.name & app.env values to use in elastic searches (Don Kidd)
        * allow for more than one ssh_keypair to be passed (Andy Farler)

2020-11-17 (1.5.6) Chris Edester
        * Set filebeat max_bytes to 1MB so we don't cause shard errors

2020-11-16 (1.5.5) Chris Edester
        * Set filebeat max_bytes to 5MB so we don't cause shard errors

2020-08-25 (1.5.4) Brad Koby
        * Increase stdlib module dependency max version
        * Increase concat module dependency max version

2020-03-03 (1.5.2) Brad Koby
        * Increase puppet-concat max version limit

2019-09-17 (1.5.1) Andrew Farler
        * Switch filebeat::prospector to filebeat::input for Filebeat 7 compatibility

2019-03-12 (1.4.10)  Brad Koby
        * Update systemd max version spec 3.0.0

2019-01-07 (1.4.9)  Dirk Tepe
        * Update filebeat version spec to get 3.3.3

2018-10-31 (1.4.8)  Dirk Tepe
        * Add process queue environment variable support

2018-10-17 (1.4.7)  Dirk Tepe
        * Make filebeat multiline marker more specific

2018-08-22 (1.4.6)  Kelly Geng
        * Restart the queue service upon app changes

2018-08-07 (1.4.5)  Dirk Tepe
        * Add Laravel Queue worker process

2018-07-25 (1.4.4)  Andrew Farler
        * use ssh_keypair hash to provide a set key pair instead of generating with ssh_keygen

2018-07-17 (1.4.3)  Kelly Geng
        * Add env key for MariaDB SSL option

2018-07-13 (1.4.2)  Chris Edester
        * Force symlinks for storage related locations
        * Add env keys for MariaDB configuration

2018-06-07 (1.4.1)  Chris Edester
        * Fix APP_SSH_KEY_PATH in laravel env

2018-06-07 (1.4.0)  Chris Edester
        * Optionally generate an ssh key for laravel apps

2018-06-01 (1.3.2)  Chris Edester
        * Manage symbolic linked storage/app, storage/log
        * Use default laravel log location with symbolic link rather than custom path

2018-03-29 (1.2.0)  Chris Edester
        * Add facts to show information about installed phpapps

2018-02-25 (1.1.2)  Dirk Tepe
        * Remove doc_type from filebeat prospector

2018-02-07 (1.1.1)  Dirk Tepe
        * Update filebeat dependency

2017-12-19 (1.1.0)  Dirk Tepe
        * Add management of generic command line app

2017-07-26 (1.0.0)  Dirk Tepe
        * Add cron job handling to Laravel app class
        * Add redis connections to env file

2017-07-22 (0.1.0)  Chris Edester
        * Update filebeat dependency
        * fix tests

2017-01-11 (0.12.0)  Dirk Tepe
        * First Stable Release

2016-12-22 (0.0.1)  Dirk Tepe
        * Initial commit
