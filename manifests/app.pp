# Define: soldel_phpapps::app
#
# Abstract a common pattern we use often.
# SolDel generic app
#

define soldel_phpapps::app (
  Enum['present', 'absent'] $ensure = present,
  $has_package            = true,
  $package                = "miamioh-php-${name}",
  $app_user               = undef,
  $app_group              = undef,
  $app_log_file           = undef,
  $base_dir               = "/var/www/apps/${name}",

  $cron_enabled           = false,
  $cron_environment       = undef,
  $cron_weekday           = '*',
  $cron_hour              = '*',
  $cron_minute            = '*',
  $cron_command           = undef,

  Hash[Enum['database', 'ldap', 'other', 'redis'], Hash] $connections = {},

  Hash $env_entries       = {},

  $manage_filebeat        = false,
  Array $filebeat_tags    = [],
){

  if $has_package and $ensure == present {
    $package_ensure = latest
    $directory_ensure = directory
  } elsif $ensure == present {
    $package_ensure = 'absent'
    $directory_ensure = directory
  } else {
    $package_ensure = 'absent'
    $directory_ensure = 'absent'
  }

  if $ensure == present and $cron_enabled {
    $cron_ensure = present
  } else {
    $cron_ensure = absent
  }

  if $name in $filebeat_tags {
    $real_filebeat_tags = $filebeat_tags
  } else {
    $real_filebeat_tags = concat($filebeat_tags, $name)
  }

  file { $base_dir:
    ensure => $directory_ensure,
    force  => true,
  }

  # lint:ignore:security_package_pinned_version
  package { $package:
    ensure => $package_ensure,
  }
  # lint:endignore

  # The .env file must be readable by the app owner and group, but
  # should not be readable by anyone else. It should not be writable
  # by anyone.
  concat { "${name}_env":
    ensure  => $ensure,
    path    => "${base_dir}/.env",
    owner   => $app_user,
    group   => $app_group,
    mode    => '0440',
    require => File[$base_dir],
  }

  concat::fragment { "${name}_env_extra":
    target  => "${name}_env",
    order   => '10',
    content => template("${module_name}/app/env_entries.erb"),
  }

  # Connection names can be reused across apps, so we need to
  # massage the hash and make sure the resource names will be
  # unique.
  # lint:ignore:variable_scope
  $connections.each |$c| {
    $real_connections = $c[1].reduce({}) |$d, $o| {
      $d + { "${name}_${o[0]}" => merge({ target => "${name}_env" }, $o[1]) }
    }
    create_resources("soldel_phpapps::connections::${c[0]}", $real_connections)
  }
  # lint:endignore

  cron { $name:
    ensure      => $cron_ensure,
    command     => $cron_command,
    environment => $cron_environment,
    user        => $app_user,
    weekday     => $cron_weekday,
    hour        => $cron_hour,
    minute      => $cron_minute,
    require     => Concat["${name}_env"],
  }
  $app_env = $::environment ? {
    'test'       => 'tst',
    'production' => 'prd',
    default      => $::environment,
  }

  if $manage_filebeat {
    filebeat::input { $name:
      ensure            => $ensure,
      paths             => [
        $app_log_file,
      ],
      max_bytes         => '1000000',
      multiline         => {
        pattern => '^\[\d{4}-\d{2}-\d{2}',
        negate  => true,
        match   => 'after',
      },
      tags              => $real_filebeat_tags,
      fields            => {
        'app.name' => $name,
        'app.env'  => $app_env,
      },
      fields_under_root => true,
    }
  }

}
