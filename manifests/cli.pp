# Define: profile::php::app::cli
# Abstract a common pattern we use often.
# SolDel Cli app
#

define soldel_phpapps::cli (
  $ensure                 = undef,
  $app_package            = undef,
  $app_user               = undef,
  $app_group              = undef,
  $app_log_file           = undef,
  $base_dir               = "/var/www/apps/${name}",

  $cron_enabled           = undef,
  $cron_environment       = undef,
  $cron_weekday           = undef,
  $cron_hour              = undef,
  $cron_minute            = undef,
  $cron_command           = undef,

  $connections            = undef,

  $env_entries            = undef,

  $manage_filebeat        = undef,
  $filebeat_tags          = undef,

  $app_name               = $name,
  Enum['debug', 'info', 'notice', 'warning', 'error', 'critical', 'alert', 'emergency'] $app_log_level = 'info',
){

  $_cron_command = $cron_command ? {
    undef   => "php ${base_dir}/bin/${app_name}.php",
    default => $cron_command,
  }

  # Use the generic app class for the basic install.
  soldel_phpapps::app { $name:
    ensure           => $ensure,
    package          => $app_package,
    app_user         => $app_user,
    app_group        => $app_group,
    app_log_file     => $app_log_file,
    base_dir         => $base_dir,
    cron_enabled     => $cron_enabled,
    cron_environment => $cron_environment,
    cron_weekday     => $cron_weekday,
    cron_hour        => $cron_hour,
    cron_minute      => $cron_minute,
    cron_command     => $_cron_command,
    connections      => $connections,
    env_entries      => $env_entries,
    manage_filebeat  => $manage_filebeat,
    filebeat_tags    => $filebeat_tags,
  }

  concat::fragment { "${name}_env_app":
    target  => "${name}_env",
    order   => '01',
    content => template("${module_name}/cli/env_app.erb"),
  }
}
