# Define: soldel_phpapps::connections::ldap
#
# Creates a env ldap section for a Laravel app
#

define soldel_phpapps::connections::ldap (
  $prefix   = 'LDAP_',
  $target   = 'env',
  $url      = undef,
  $server   = undef,
  $port     = undef,
  $username = undef,
  $password = undef,
  $basedn   = undef,
){

  concat::fragment { "env_ldap_${name}":
    target  => $target,
    order   => '03',
    content => template("${module_name}/connections/env_ldap.erb"),
  }

}
