# Define: soldel_phpapps::connections::database
#
# Creates a env database section for a Laravel app
#

define soldel_phpapps::connections::database (
  $prefix      = 'DB_',
  $target      = 'env',
  $connection  = undef,
  $tns         = undef,
  $host        = undef,
  $database    = undef,
  $username    = undef,
  $password    = undef,
  $ssl_ca_path = undef,
){

  concat::fragment { "env_db_${name}":
    target  => $target,
    order   => '02',
    content => template("${module_name}/connections/env_db.erb"),
  }

}
