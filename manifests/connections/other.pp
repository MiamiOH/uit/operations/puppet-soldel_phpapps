# Define: soldel_phpapps::connections::other
#
# Creates a env other section for a Laravel app
#

define soldel_phpapps::connections::other (
  $prefix      = 'OTH_',
  $target      = 'env',
  Hash $values = {},
){

  concat::fragment { "env_other_${name}":
    target  => $target,
    order   => '04',
    content => template("${module_name}/connections/env_other.erb"),
  }
}
