# Define: soldel_phpapps::connections::redis
#
# Creates a env redis section for a Laravel app
#

define soldel_phpapps::connections::redis (
  $prefix   = 'REDIS_',
  $target   = 'env',
  $host     = undef,
  $password = undef,
  $port     = undef,
){

  concat::fragment { "env_redis_${name}":
    target  => $target,
    order   => '02',
    content => template("${module_name}/connections/env_redis.erb"),
  }

}
