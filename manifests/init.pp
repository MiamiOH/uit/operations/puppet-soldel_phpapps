# Class: soldel_phpapps
#
# This class installs and manages Solution Delivery PHP applications

class soldel_phpapps (
  $app_user           = undef,
  $app_group          = undef,
  Hash $apps          = {},
  Hash $laravel_apps  = {},
  Hash $cli_apps      = {},
) inherits soldel_phpapps::params {

  create_resources('soldel_phpapps::app', $apps)
  create_resources('soldel_phpapps::cli', $cli_apps)
  create_resources('soldel_phpapps::laravel', $laravel_apps,
    {
      app_user  => $app_user,
      app_group => $app_group,
    }
  )

}
