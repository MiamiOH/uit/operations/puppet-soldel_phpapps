# Define: profile::php::app::laravel
#
# Abstract a common pattern we use often.
# SolDel Laravel app
#

define soldel_phpapps::laravel (
  $ensure                 = present,
  $has_package            = true,
  $app_package            = "miamioh-php-${name}",
  $app_user               = undef,
  $app_group              = undef,
  $base_dir               = "/var/www/apps/${name}",
  Stdlib::Absolutepath $storage_path     = "/var/www/storage/${name}",
  Stdlib::Absolutepath $storage_log_path = "/var/log/phpapps/${name}",
  $has_public_storage     = false,

  $queue_enabled          = false,
  $queue_options          = undef,
  $queue_environment      = [],

  $cron_enabled           = undef,
  $cron_environment       = undef,
  $cron_weekday           = undef,
  $cron_hour              = undef,
  $cron_minute            = undef,
  $cron_command           = undef,

  Hash $ssh_keypair       = {},

  $connections            = undef,

  $env_entries            = undef,

  $manage_filebeat        = undef,
  $filebeat_tags          = undef,

  $app_name               = $name,
  $app_key                = undef,
  $app_debug              = false,
  $app_url                = "https://${::fqdn}",
  $web_root               = '/var/www/html',
  Enum['local', 'development', 'test', 'production'] $app_env = 'local',
  Enum['debug', 'info', 'notice', 'warning', 'error', 'critical', 'alert', 'emergency'] $app_log_level = 'info',

  $manage_jwt_secret      = false,
  $jwt_secret             = undef,
){

  # Used in filebeat config. Assume the normal Laravel log location.
  $app_log_file = "${base_dir}/storage/logs/laravel.log"
  $ssh_key_path = "${storage_path}/app/.ssh/id_rsa"

  $_cron_command = $cron_command ? {
    undef   => "php ${base_dir}/artisan schedule:run >> /dev/null 2>&1",
    default => $cron_command,
  }

  # expose package as web app
  if $has_package and $ensure == present {
    $link_ensure = link
    $web_link_ensure = link
    $directory_ensure = directory
    # no package, just config to run a container
  } elsif $ensure == present {
    $link_ensure = link
    $web_link_ensure = 'absent'
    $directory_ensure = directory
  } else {
    $link_ensure = $ensure
    $web_link_ensure = $ensure
    $directory_ensure = $ensure
  }

  # Use the generic app class for the basic install.
  soldel_phpapps::app { $name:
    ensure           => $ensure,
    has_package      => $has_package,
    package          => $app_package,
    app_user         => $app_user,
    app_group        => $app_group,
    app_log_file     => $app_log_file,
    base_dir         => $base_dir,
    cron_enabled     => $cron_enabled,
    cron_environment => $cron_environment,
    cron_weekday     => $cron_weekday,
    cron_hour        => $cron_hour,
    cron_minute      => $cron_minute,
    cron_command     => $_cron_command,
    connections      => $connections,
    env_entries      => $env_entries,
    manage_filebeat  => $manage_filebeat,
    filebeat_tags    => $filebeat_tags,
  }

  concat::fragment { "${name}_env_app":
    target  => "${name}_env",
    order   => '01',
    content => template("${module_name}/laravel/env_app.erb"),
  }

  # The app is deployed by symlinking the Laravel public directory
  # into the web doc root.
  file { "${web_root}/${app_name}":
    ensure  => $web_link_ensure,
    target  => "${base_dir}/public",
    require => File[$base_dir],
  }

  file { [$storage_path, "${storage_path}/app"]:
    ensure  => $directory_ensure,
    owner   => $app_user,
    group   => $app_group,
    mode    => '0755',
    force   => true,
    seltype => 'httpd_sys_rw_content_t',
  }

  file { $storage_log_path:
    ensure  => $directory_ensure,
    owner   => $app_user,
    group   => $app_group,
    mode    => '0750',
    force   => true,
    seltype => 'httpd_log_t',
  }

  # Laravel writes logs and other file system based transactional
  # data into the storage and cache directories.
  if $ensure == 'present' {

    $public_storage_link_ensure = $has_public_storage ? {
      true    => link,
      default => absent,
    }

    file { [
        "${base_dir}/bootstrap",
        "${base_dir}/bootstrap/cache",
        "${base_dir}/storage",
        "${base_dir}/storage/framework",
        "${base_dir}/storage/framework/cache",
        "${base_dir}/storage/framework/sessions",
        "${base_dir}/storage/framework/views",
      ]:
        ensure  => directory,
        owner   => $app_user,
        group   => $app_group,
        mode    => '0755',
        require => File[$base_dir],
        seltype => 'httpd_sys_rw_content_t',
    }

    file { "${base_dir}/storage/app":
      ensure  => $link_ensure,
      owner   => $app_user,
      group   => $app_group,
      target  => "${storage_path}/app",
      force   => true,
      require => File[$base_dir],
    }

    # Laravel suggests making files public by symlinking the public
    # directory from the local storage into the public web space
    file { "${storage_path}/app/public":
      ensure  => directory,
      owner   => $app_user,
      group   => $app_group,
      mode    => '0755',
      seltype => 'httpd_sys_rw_content_t',
    }

    unless empty($ssh_keypair) {
      file { "${storage_path}/app/.ssh":
        ensure  => directory,
        owner   => $app_user,
        group   => $app_group,
        mode    => '0700',
        seltype => 'httpd_sys_content_t',
      }
      $ssh_keypair.each |$label, $keypair| {
        if ($label == 'default') {
          $keypath = "${storage_path}/app/.ssh/"
        }
        else {
          $keypath = "${storage_path}/app/.ssh/${label}_"
        }

        file { "${keypath}id_rsa":
          ensure  => file,
          owner   => $app_user,
          group   => $app_group,
          mode    => '0600',
          seltype => 'httpd_sys_content_t',
          content => $keypair['private'],
        }
        file { "${keypath}id_rsa.pub":
          ensure  => file,
          owner   => $app_user,
          group   => $app_group,
          mode    => '0644',
          seltype => 'httpd_sys_content_t',
          content => $keypair['public'],
        }
      }
    }

    file { "${base_dir}/public/storage":
      ensure  => $public_storage_link_ensure,
      owner   => $app_user,
      group   => $app_group,
      target  => "${base_dir}/storage/app/public",
      force   => true,
      require => Soldel_phpapps::App[$name],
    }

    file { "${base_dir}/storage/logs":
      ensure  => $link_ensure,
      owner   => $app_user,
      group   => $app_group,
      target  => $storage_log_path,
      force   => true,
      require => File[$base_dir],
    }
  }

  if ($ensure == present and $queue_enabled) {
    $unit_ensure    = present
    $service_ensure = running
    $service_enable = true
  } else {
    $unit_ensure    = absent
    $service_ensure = stopped
    $service_enable = false
  }

  $queue_name = "laravel-queue-${name}"

  $queue_environment_vars = $queue_environment ? {
    String  => [ $queue_environment ],
    Array   => $queue_environment,
    undef   => [],
    default => fail('soldel_phpapps::laravel - Unsupported queue environment value'),
  }

  if $::service_provider == 'systemd' {
    systemd::unit_file { "${queue_name}.service":
      ensure  => $unit_ensure,
      content => template("${module_name}/laravel/queue.service.erb"),
      notify  => Service[$queue_name],
    }

    service { $queue_name:
      ensure     => $service_ensure,
      enable     => $service_enable,
      hasstatus  => true,
      hasrestart => true,
      subscribe  => Soldel_phpapps::App[$name],
    }

  }

}
