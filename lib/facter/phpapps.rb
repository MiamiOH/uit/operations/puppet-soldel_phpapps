# Fact: phpapps
#
# Purpose: make facts for each deployed php app that shows useful information
#
# Resolution:
#   reads each app from /var/www/apps and returns a hash with:
#   * files: the files in the app folder.
#   * env: the contents of .env, scrubbed of any sensitive content.
#     - For the sensitive content, I think it is sufficient to just check for a list of known keys
#       and update it as necessary. No need to go wild on that.
#   * version: shows the rpm version installed
#
# Caveats:
#   Makes these assumptions:
#   * all apps are in /var/www/apps
#   * all app rpms names are miamioh-php-${name}
#   * we have a known list of password fields to scrub from .env
#
# Notes:
#   Many developers request this info on a regular basis...

BASE_DIR = '/var/www/apps'.freeze
SENSITIVE_ENV = ['PASSWORD', 'SECRET', 'KEY', 'TOKEN'].freeze

def installed_phpapps
  if Facter::Util::Resolution.which('rpm')
    rpm_query = %(rpm -qa --queryformat "%{name}\n" "miamioh-php-*")
    Facter::Util::Resolution.exec(rpm_query).split("\n")
  else
    []
  end
end

def add_fact_for_app(app)
  app_name = app.sub('miamioh-php-', '')
  app_map = { 'webdirectory' => 'directory' }
  dir_name = app_map[app_name] || app_name
  app_dir = File.join(BASE_DIR, dir_name)
  fact_hash = {}

  fact_hash[:files] = Dir.entries(app_dir)

  app_env = File.read(File.join(app_dir, '.env'))
  SENSITIVE_ENV.each { |se| app_env.gsub!(%r{^([^=]*#{se}[^=]*)=.*$}, '\1=[redacted]') }
  fact_hash[:env] = app_env.split("\n")

  rpm_query = %(rpm -q --queryformat "%{version}-%{release}\n" #{app})
  fact_hash[:version] = Facter::Util::Resolution.exec(rpm_query)

  sshdir = File.join(app_dir, 'storage', 'app', '.ssh')
  ['rsa'].each do |keytype|
    pubpath = File.join(sshdir, "id_#{keytype}.pub")

    next unless File.file?(pubpath)
    full = File.read(pubpath).chomp
    type, key, comment = full.split($FS, 3)
    fact_hash[:ssh] ||= {}
    fact_hash[:ssh][:"#{keytype}"] = { full: full, type: type, key: key, comment: comment }
  end

  Facter.add(:"phpapp-#{app_name}") do
    setcode { fact_hash }
  end
end

installed_phpapps.each { |app| add_fact_for_app(app) }
