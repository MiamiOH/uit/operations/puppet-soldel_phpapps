require 'securerandom'

# generate a base64 Laravel app key
module Puppet::Parser::Functions
  newfunction(:laravel_app_key, type: :rvalue, doc: <<-DESCRIPTION
                Returns a base64 string suitable for the Laravel app key.
                DESCRIPTION
  ) do |args|
    unless args.empty?
      raise(Puppet::ParseError, 'laravel_app_key(): Wrong number of arguments ' \
        "given (#{args.size} for 0)")
    end

    'base64:' + SecureRandom.base64(32)
  end
end
